# **PIM Product / Modification UI**

### **Product**
<p>
Lorsqu'on récupère une valeur d'un produit : valeur de type ValueInterface(). <br>
Si scopable / localisable : value multipliée. <br />
Chaque valeur de type ValueInterface implémente une méthode toString() permettant de rendre la donnée.
</p>

#### **Query Builder Elasticsearch**
<p>Lors de la création d'une commande utilisant les query builder Akeneo, 
préfixé par "pim" afin de ne pas avoir le pb de token. <br/>
--> Pour les commandes préfixées par "pim", un listener crée une session et set un token.
</p>

#### **Filtre sur les QB**
<p>
    Méthode : addFilter() <br />
    Note pour les filtres : pim:product:query-help <br />
</p>
Exemple : 

```php
<?php
    $pqbFactory = $this->getContainer()->get('pim_catalog.query.product_query_builder_factory');
    $pqb = $pqbFactory->create([
        'default_locale' => 'en_US',
        'default_scope'  => 'ecommerce',
    ]);
    $pqb->addFilter('categories', Operators::IN_CHILDREN_LIST, ['cameras']);
    ...
?>
```
#### **Tri sur les résultats de la QB**
<p>
    Méthode : addSorter() <br />
</p>
Exemple : 

```php
<?php
    ...
    $pqb->addSorter('description', Directions::ASCENDING, [
        'locale' => 'en_US',
        'scope'  => 'print'
    ]);
    ...
?>
```
#### **Modification sur les PIM Product**
<p>
    Service : Pim\Component\Catalog\Updater\ProductUpdater. <br/>(chaque entité du PIM à son propre Updater)
</p>

```php
<?php
    ...
    $valid = [ 
        'values' => [
            'training' => [[
                'locale' => null,
                'scope' => null,
                'data' => 'Hell'
            ]]
        ]
    ];
    /** @var Pim\Component\Catalog\Updater\ProductUpdater $updater */
    $updater = $this->getContainer()->get('pim_catalog.updater.product');
    $validator = $this->getContainer()->get('pim_catalog.validator.product');
    $saver = $this->getContainer()->get('pim_catalog.saver.product');
    $updater->update(
        $product,
        $valid
    );
    $violations = $validator->validate($product);
    if ($violations->count() <= 0) {
        exit;
    }  else {
      // 1ère phase pour libérer de la mémoire
      $this->getContainer()->get('akeneo_storage_utils.doctrine.object_detacher')->detach($product); // On détache le produit non-valide
    }
    
    $saver->save($product);
    ...
?> 
``` 
<p>
NOTES : Il faut penser à détacher les objets 
</p>

### **Customize the UI**

Documentation Akeneo : [LIEN](https://docs.akeneo.com/latest/design_pim/)

- RequireJS / BackboneJS
- Dossier : Resources/public/js || Resources/public/templates

<p>
    Etapes : 
   <ul> 
    <li> Création de fichier JS et template</li>
    <li> Définit nos clés dans Resources/config/requirejs.yml </li>
    <li> On construit la page avec notre module dans form_extensions </li>
    <li> Rafraîchit les caches / Reconstruit les assets </li>
   </ul>
</p>
