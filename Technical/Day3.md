# **Surcharge Entité PIM / Référence Data / Jobs**

### **Surcharge Entité PIM**
Mapping YAML : 
```yaml
changeTrackingPolicy: DEFERRED_EXPLICIT
```
A ajouter lors de la définition/surcharge d'un mapping d'une entité. 
Permet de libérer de la mémoire lors des actions Doctrine.

**!! Attention !!** : Lors de la modification d'une entité, penser si y'a des répercussions sur d'autres pages.  

Voir documentation surcharge de l'entité PimCategory : [Clique ici](https://docs.akeneo.com/2.2/manipulate_pim_data/category/add_new_properties_to_a_category.html#configure-the-mapping-override)

### **Reference Data**
Voir documentation création d'une reference data : [Clique ici](https://docs.akeneo.com/2.2/manipulate_pim_data/catalog_structure/creating_a_reference_data.html)

**!! Attention !!** : 
- Reference Data ne gère pas les traductions, à intégrer nativement. (copier fonctionnement des PIM Entities)

Déclarer son entité comme Reference Data dans app/config.yml : 
```yaml
pim_reference_data:
    supplier: (si #multiple, ajout d'un 's')
        class: Training\Bundle\SupplierBundle\Entity\Supplier
        type: simple (ou #multiple)
```

Ensuite aller dans Settings/Attributes et voir notre ref dans : pim_reference_data_simpleselect

Pour l'affichage dans l'UI, surchargé : 
```php 
public static function getLabelProperty()
{
    return 'name'; // reference to $this->name;
}
```

Vérifier l'intégrité de la référence data :
```bash
php bin/console pim:reference-data:check
```

**Intégration de la référence data dans l'UI du PIM :**

Installer le bundle d'Akeneo Labs : 

```php 
composer require 'akeneo-labs/custom-entity-bundle 2.3.*'
```

Activer le bundle dans AppKernel : 

```php 
new \Pim\Bundle\CustomEntityBundle\PimCustomEntityBundle() 
```

Ajouter le routing du bundle : 

```yml 
akeneo_custom_entity: 
    resource: "@PimCustomEntityBundle/Resources/config/routing.yml"
```

**Plugger le bundle avec la référence data d'Akeneo.**

Etendre l'entité : 

```php
<?php 
class Supplier extends AbstractCustomEntity {
    ...
    public function getCustomEntityName(): string
    {
        return 'supplier';
    } 
    ...
}
```

Mise à jour du repository :

```php
<?php 
class SupplierRepository extends CustomEntityRepository
{
}
```

Ajout de la grille :

Voir commit 

Implanter notre propre Normalizer afin de recracher la donnée :

```php
<?php 
class SupplierNormalizer implements NormalizerInterface
{
    /** @var array $supportedFormats */
    protected $supportedFormats = [
        'standard'
        // 'internal_api',
        // voir doc
    ];

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $normalizedEntity = [
            'id'        => $object->getId(),
            'code'      => $object->getCode(),
            'name'      => $object->getName(),
            'email'     => $object->getEmail(),
            'is_active' => $object->isActive(),
        ];

        return $normalizedEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Supplier && in_array($format, $this->supportedFormats);
    }
}
```
Resources/config/normalizers.yml : 
```yml
services: 
    training.normalizer.supplier_normalizer: 
        class: Training\Bundle\SupplierBundle\Normalizer\SupplierNormalizer
        tags:
            - { name: pim_serializer.normalizer, priority: 200 }
```

### **Import/Export Job Connector**

Pour retrouver les form providers : ``` @PimEnrichBundle/Resources/config/providers.yml ``
Configuration de la UI du Job Custom :
```yaml
# Resources/config/providers.yml
services: 
    training.supplier_import.form.job_instance:
        class: '%pim_enrich.provider.form.job_instance.class%'
        arguments:
            -   # code du job / clé de la form extension à utiliser
                csv_supplier_import: pim-job-instance-csv-base-import
        tags:
            - { name: pim_enrich.provider.form }
```
```yaml
# Resources/config/forms.yml

# Définit le formulaire
training.supplier_import.form.job_instance:
    class: '%pim_enrich.provider.form.job_instance.class%'
    arguments:
        -   # code du job / clé de la form extension à utiliser
            csv_supplier_import: pim-job-instance-csv-base-import
    tags:
        - { name: pim_enrich.provider.form }


# Définit les valeurs par défaut
training.supplier_import.default_value_provider:
    class: '%pim_connector.job.job_parameters.default_values_provider.simple_csv_import.class%'
    arguments:
        -
            - 'csv_supplier_import' # Fait référence à la clé du formulaire définit dans le provider (ligne 23)
    tags:
        - { name: akeneo_batch.job.job_parameters.default_values_provider }


# Définit les contraintes d'intégrité du formulaire
training.supplier_import.constraint_collection_provider:
    class: '%pim_connector.job.job_parameters.constraint_collection_provider.simple_csv_import.class%'
    arguments:
        -
            - 'csv_supplier_import' # Fait référence à la clé du formulaire définit dans le provider (ligne 23)
    tags:
        - { name: akeneo_batch.job.job_parameters.constraint_collection_provider }
```
Pour un mapping, implémenter nos propres ArrayConverters au niveau du reader :
```yaml
# readers.yml
training.reader.file.csv.supplier:
    class: '%pim_connector.reader.file.csv.class%'
    arguments:
        - '@pim_connector.reader.file.csv_iterator_factory'
        - '@training.array_converter.flat_to_standard.supplier'

training.array_converter.flat_to_standard.supplier:
    class: Training\Bundle\SupplierBundle\ArrayConverter\FlatToStandard\Supplier
    arguments:
        - '@pim_connector.array_convertor.checker.fields_requirement'

# Training\Bundle\SupplierBundle\ArrayConverter\FlatToStandard\Supplier.php
class Supplier implements ArrayConverterInterface
{
    /** @var FieldsRequirementChecker */
    protected $fieldChecker;

    /**
     * @param FieldsRequirementChecker $fieldChecker
     */
    public function __construct(FieldsRequirementChecker $fieldChecker)
    {
        $this->fieldChecker = $fieldChecker;
    }

    public function convert(array $item, array $options = [])
    {
        $this->fieldChecker->checkFieldsPresence($item, ['code']);
        $this->fieldChecker->checkFieldsFilling($item, ['code']);

        return $item;
    }
}
```

Persistance de la données :

``` yaml
training.writer.database.supplier:
    class: '%pim_connector.writer.database.class%'
    arguments:
        - '@pim_custom_entity.saver.custom_entity'
        - '@akeneo_storage_utils.doctrine.object_detacher'
```