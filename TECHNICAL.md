# Formation Technique Akeneo

Intervenante : Marie - Consultante technique

En long et en large : [Akeneo Documentation](https://docs.akeneo.com/2.2/manipulate_pim_data/mass_edit.html)

- **[Jour 1](/Technical/Day1.md)** : Formation - Day 1
- **[Jour 2](/Technical/Day2.md)** : Formation - Day 2
- **[Jour 3](/Technical/Day3.md)** : Formation - Day 3
- **[Jour 4](/Technical/Day4.md)** : Formation - Day 4
- **[Best Practices](/Technical/BestPractices.md)** : Tips de Marie dans les best practices Akeneo
- **[Certification](/Technical/CERTIF.md)** : Quelques conseils et astuces sur la certification technique
