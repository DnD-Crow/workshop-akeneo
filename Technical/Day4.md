# **PIM API / Rules**

### **PIM API : Utilisation**

Voir documentation API Akeneo : [Clique ici](https://api.akeneo.com/getting-started-postman-collection.html)

Voir Controller : [Clique ici](https://gitlab.com/DnD-Crow/pim-ee-formation/blob/master/src/Training/Bundle/SupplierBundle/Controller/Rest/Api/SupplierController.php)

Création des identifiants OAUTH : 
- Via l'UI : System > API Connections
- Via le bash : 
```bash 
php bin/console pim:oauth-server:create-client my-customer-account \
    --grant_type="password" \
    --grant_type="refresh_token" \
    --env=prod
```
**!! Attention !!** : Bonne pratique de créer un utilisateur pour l'API pour faire varier les permissions.

Configurer Postman après import des templates/collections d'Akeneo : 
- Mettre à jours les credentials via le template d'Akeneo : ```url / client_id / secret / username / password```
- Générer un token d'accès pour générer une session : ```http://127.0.0.1:8000/api/oauth/v1/token```

### **Création d'endpoint**

Créer nos routes customs : 
```yaml
# app/config/routing.yml
training_supplier:
    resource: "@TrainingSupplierBundle/Resources/config/routing.yml"
    prefix: /api/rest/v1
    
# src/Bundle/Resources/config/routing.yml
supplier_api_get:
    path: /suppliers/{code}
    defaults: { _controller: TrainingSupplierBundle:Rest\Api\Supplier:get }
    methods: [GET]
# Ou par un service: 
suppliers_api_get:
    path: /suppliers
    defaults: { _controller: training_api.controller.supplier:getAction }   
    methods: [GET]
```

Normalizer les données de sorties: 

On utilise le serializer d'Akeneo et on normalize au format : "external_api"

@Voir Compiler Pass Symfony

### **Rules**

Import de la rule :
```yaml
rules:
    training_sale_price:
        conditions:
            - field: family
              operator: 'IN'
              value:
                  - webcams
            - field: price
              operator: 'NOT EMPTY ON AT LEAST ONE CURRENCY'
        actions:
            # On définit les clés à la volée / valeur de l'attribut dans le PIM
            - type: sale                        # Clé de l'action --> définit le nom du model : Training\Bundle\RulesBundle\Model\SaleAction
              base_price_attribute: price       # Attribut PIM existant
              sale_price_attribute: sale_price  # Attribut PIM non-existant --> on le crée
              percentage: 20                    # Valeur en dure
              currencies: ['EUR', 'USD']
```

Définition de la rule :
```yaml
services:
    training.rule.dernormalizer.sale_price:
        class: '%pimee_catalog_rule.denormalizer.product_rule.action.class%'
        arguments:
            # Référence du model lié à la rules
            - 'Training\Bundle\RulesBundle\Model\SaleAction'
            # La clé "type" de l'action la rules
            - 'sale'
        tags:
            - { name: pimee_catalog_rule.denormalizer.product_rule }

````

Mapping des attributs de l'action de la rule :

```php 
<?php 

/**
 * SaleAction constructor.
 * @param $data --> référence au data reçu dans le fichier YAML
 */
public function __construct($data)
{
    $this->basePriceAttribute = $data['base_price_attribute'];
    $this->salePriceAttribute = $data['sale_price_attribute'];
    $this->percentage = $data['percentage'];
    $this->currencies = $data['currencies'];
}
```
