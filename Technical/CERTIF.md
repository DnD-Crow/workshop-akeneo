# Certification Akeneo

## Résumé :
- Durée : 1h30, bientôt 2h00
- Partie QCM / partie écrite
- **Pour la partie QCM :**
- **Pour la partie écrite :**
    - Pas besoin de faire des phrases, il faut trouver les mots précis/justes.
    - On peut écrire en français. (questions en anglais)

## Contenu :
- Symfony.
- Couche Akeneo/PIM : fonctionnelle et technique.
- Concerne la version CE / EE.
- Pas mal de questions sur des uses cases et si on peut le faire nativement ou non.
- Analyse de code.
- Stratégie : 
    - 20% Architecture
    - 25% Entité PIM
    - 20% import/export
    - 15% EE (fonctionnalités)
    - 20% UI (fonctionnelle/dashboard)

## Exemple : 
- Quels sont les étapes pour ...
- Comment récupérer ... 
- Quels sont les requirements du PIM ? 

## Tips : 
- Auditer du code.
- Faire des workshops entre nous.

## Bilan : 
**!! Résultat !!** :
- Atteindre += de 80%
- Correction fourni chez Akeneo.
- Réponse par mail.

**!! Attention !!** : 
- Plus de code à écrire.
- Plus de points dégressifs. 