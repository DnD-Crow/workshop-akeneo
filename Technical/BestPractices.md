# Development

- Pas besoin de découper les configs en petits fichiers (normalizers, entities etc...)
- Pas besoin de faire pleins de bundle si petites modifs.

- MediaBundle : Découper en steps. 
- RuleBundle : Pas besoin de redéfinir le _action.html.twig

- AS400Bundle : Penser à détacher quand l'objet est invalidé.

- Pim/Component/Catalog/Normalizer/Api/***


Pas d'export dans /web