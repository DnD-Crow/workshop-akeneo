# Formation Fonctionnelle Akeneo 2.2.x [15/05/18 - 17/05/18]

Intervenante : Nathalie - Consultante fonctionnelle

- **[Jour 1](#jour1)** : Concepts clés
- **[Jour 2](#jour2)** : Produits variants
- **[Jour 3](#jour3)** : Structurer le catalogue et configurer les référentiels de données

---
## <a name="jour1">Jour 1</a>

### **Partie théorique/présentation**

**But d'un PIM** : 
1. Collecter
2. Enrichir et contrôler les produits
3. Diffuser les infos vers différents canaux

/!\ Le but n'est pas d'avoir toutes les informations

Le PIM contient des _données froides_ : Il stocke des données persistantes, il vaut mieux éviter des attributs comme le prix.

- **Canal** : Destination des informations d'un produit
- **Locale** : Combinaison d'une langue et d'un pays
- **Arbres/Categories** : Aider l'organisation des produits (les permissions sur les catégories sont dans la EE)
- **Attributs** : Les différents champs d'information d'un produit (organisés par groupes (permissions dans la EE))
  - Un attribut peut être spécifique à une locale ou à un canal (ces choix sont difficilement changeables après création)
- **Famille** : Jeu d'attributs. Un produit dans une famille aura les attributs spécifiés à cette famille. Gère la complétude
- **Complétude** : On nomme un produit comme complet lorsque les attributs qui définissent la complétude sont renseignés. Elle se définit par Famille/Canal/Locale

### **Partie démonstration**

**Dashboard** : (parties que je connais pas vraiment)
- Projets (EE)
- [Workflow de propositions/validation](#prop_val) (EE)

**Projets (EE)** : 
- Définir un objectif de complétude de certains produits avec un objectif et une date de finalisation

**Grille produits** :
- Les filtres ont été pensés pour être aussi puissants que Excel
- Affichage en grille plutôt que liste
- On peut configurer les colonnes affichées (et créer des vues pour sauvegarder les choix de colonnes et de filtres)

**Fiche produit** :
- Possibilité de "comparer/traduire" un produit en fonction des différentes locales et canaux
- Complétude par canal et locale et indication des attributs manquants (possibilité de filtrer par attributs manquants)
- PAM (Product Asset Manager - Gestion des ressources) (EE)
- Possibilité d'ajouter des options de select depuis la fiche produit
- Possibilité d'exporter la fiche en PDF

**Attributs** :
- Définir des paramètres de validation (on peut créer des regex custom aussi)

**Les actions de masse sur les produits** (11 actions au total) :
- Editer un attribut commun sur plusieurs produits (gain de temps)
- Ajouter une valeur dans un sélecteur à choix multiple et dans une collection de ressources
- Changer la famille de plusieurs produits en même temps
- Faire des associations

**Pour être exporté un produit doit être** :
- Complet
- Être dans une catégorie
- En statut actif

**Associations de produits**
- Ne se fait que dans un sens (a->b != b->a)
- Possibilité d'association 1 vers n

**Workflows** :
- Standard : produit complet, avec categorie, actif
- <a name="prop_val">Proposition/validation</a> : On enrichit un produit qui sera envoyé en brouillon et validé ou rejeté ensuite (Bouton de "soumission" sur un produit)
- Workflow de publication : Un produit qui a une version figée, et un clone qui sera modifié en continu. Il faudra republier le produit pour appliquer les modifications en attente. Par contre on voit les dernière modification même non publiées dans la grille

**Connecteurs**
- Importer/Exporter vers des applications externes (i.e. Pimgento)

**Import/Export**
- Les formats attendus sont les mêmes

---
## <a name="jour2">Jour 2</a>

[Doc Produits variants](https://help.akeneo.com/articles/what-about-products-variants.html)

<a href="https://help.akeneo.com/img/scheme_variants.png" target="_blank"><img src="https://help.akeneo.com/img/scheme_variants.png" width="600px"></a>

**Famille de variants** : sert  à définir la structure pour des produits avec des variations :
- Dans une famille, rajouter une variante dans le menu adapté en spécifiant les axes

---
## <a name="jour3">Jour 3</a>

**Reference data** :
- Table de données composée d'attributs
- Un développement technique est nécessaire
- Ex : des pictos pour le lavage d'habits

**Attribut calculé (EE)** :
- Attribut qui est enrichit automatiquement grâce à une règle
- Si plusieurs conditions `OU`, préférer plusieurs règles `ET`
- Préférer les attributs "utilisables sur la grilles" pour améliorer les performances
- Règles déclenchées par cron, par import ou après édition de masse (ou manuellement)
- Sur les pages produit, ces attributs spéciaux sont indiqués

### **Les droits**

Les droits s'appuient sur les rôles et les groupes.

**Rôles** :
- Définir un ensemble de droits au niveau des actions
- Plusieurs rôles possibles pour un même utilisateur
  - Les droits sont cumulatifs et les plus permissifs sont appliqués

**Groupes** :
- Définit des droits d'accès (onglet `Permissions`) au niveau des 
  - Locales
  - Catégories
  - Groupes d'attributs
- Plusieurs groupes possibles pour un même utilisateur

### **Gestion du catalogue**

**Locales** :
- Pour activer une locale -> l'ajouter à un canal

**Ordre à la création dun PIM Akeneo** :
1. Devises
1. Canaux
1. Grp attributs
1. Attributs
1. Options d'attributs
1. Familles